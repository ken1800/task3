import { Models } from './models';
import { Roles } from '@prisma/client';
type User = { id: number; iat: number; roles: Roles };

type ContextModule = {
  models: Models;
  user: User;
};

export { ContextModule };
