import { db } from '../../db';

export const findUserLoans = async (id: number) => {
  return await db.loan.findMany({ where: { updatorId: id } });
};

export const getUserPayments = async (id: number) =>
  await db.payment.findMany({
    where: {
      updatorId: id,
    },
  });
