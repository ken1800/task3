/** @format */

import { unionType, objectType, list } from 'nexus';

export const FindUserResponse = unionType({
  name: 'FindUserResponse',
  description: 'Response with user(s) or error',
  resolveType: (data) => {
    console.log({ data });
    const __typename = 'id' in data ? 'User' : 'Error';
    return __typename;
  },
  definition(t) {
    t.members('User', 'Error');
  },
});

export const SendEmailResponse = unionType({
  name: 'SendEmailResponse',
  description: 'Response with user(s) or error',
  resolveType: (data) => {
    const __typename = 'message' in data ? 'Error' : 'EmailSuccess';
    return __typename;
  },
  definition(t) {
    t.members('EmailSuccess', 'Error');
  },
});

export const EmailSuccess = objectType({
  name: 'EmailSuccess',
  definition(t) {
    t.boolean('EmailSuccess');
  },
});

export const User = objectType({
  name: 'User',
  definition(t) {
    t.string('fname');
    t.nullable.int('roleId');
    t.string('lname');
    t.boolean('isActive');
    t.string('password');
    t.string('email');
    t.nullable.int('updatorId');
    t.date('createdAt');
    t.date('updatedAt');
    t.boolean('isActive');
    t.nonNull.int('id');
    t.nullable.field('roles', {
      type: 'RolesResult',
    });
  },
});

export const BatchPayload = objectType({
  name: 'BatchPayload',
  definition(t) {
    t.int('count');
  },
});

export const BatchPayloadResponse = unionType({
  name: 'BatchPayloadResponse',
  description: 'BatchPayloadResponse response model',
  resolveType: (data) => {
    const __typename = 'count' in data ? 'BatchPayload' : 'Error';

    return __typename;
  },
  definition(t) {
    t.members('BatchPayload', 'Error');
  },
});
