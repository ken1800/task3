import { Prisma } from '@prisma/client';
import { db } from '../../db';

export type LoanCreateInput = {
  amount: number;
  isApproved?: boolean;
  payMentDate: Date | string;
  createdAt?: Date | string;
  updatedAt?: Date | string;
  isActive?: boolean;
  updatorId: number;
};

export type PaymentCreate = {
  amount: number;
  balance: number;
  loanId: number;
  updatorId: number;
};
export const requestLoan = (loanInput: LoanCreateInput) => {
  return db.loan
    .create({
      data: {
        ...loanInput,
      },
    })
    .then((res) => res)
    .catch((err) => ({ message: 'error adding Loan' }));
};

export const addPayment = (payment: PaymentCreate) =>
  db.payment
    .create({
      data: {
        ...payment,
      },
    })
    .then((res) => res)
    .catch((error) => new Error(error));
