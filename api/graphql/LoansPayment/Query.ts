import { queryField, list, intArg, nonNull } from 'nexus';
import { getUserPayments, findUserLoans } from '../../helpers/LoansAndPayment';

export const getUserLoans = queryField('userLoans', {
  type: list('LoanResponse'),
  args: {
    id: nonNull(intArg()),
  },
  resolve(_root, args, {}) {
    return findUserLoans(args.id);
  },
});

export const getUserPayment = queryField('userPayments', {
  type: list('PaymentResponse'),
  args: {
    id: nonNull(intArg()),
  },
  resolve(_root, args, {}) {
    return getUserPayments(args.id);
  },
});
