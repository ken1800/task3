import { extendType, nonNull, intArg, nullable, booleanArg, list } from 'nexus';
import {
  UpdateUserRole,
  toggleActivateUser,
  forgotPassword,
  createNewUser,
  changePassword,
} from '../../helpers/User';

export const UpdateRoleMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateUserRole', {
      type: 'User',

      args: {
        id: nonNull(intArg()),
        roleId: nullable(intArg()),
      },
      resolve(_root, args, { models }) {
        return UpdateUserRole(args);
      },
    });
  },
});

export const toggleActiveUser = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('toggleActiveUser', {
      type: 'BatchPayloadResponse',

      args: {
        activate: nonNull(booleanArg()),
        ids: list(nonNull(intArg())),
      },
      resolve(_root, args, { models }) {
        const { ids, activate } = args;
        return toggleActivateUser({ activate })({ ids }).fork(
          (x) => x,
          (x) => x
        );
      },
    });
  },
});

export const createUserMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('createUserMutation', {
      type: 'SendEmailResponse',
      args: {
        lname: nonNull('String'),
        fname: nonNull('String'),
        password: nonNull('String'),
        email: nonNull('String'),
        updatorId: nullable('Int'),
      },
      resolve(_root, args, { models }) {
        return createNewUser(args).fork(
          (error) => error,
          (emailSuccess) => emailSuccess
        );
      },
    });
  },
});

export const ForgotPasswordMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('forgotPassword', {
      type: 'SendEmailResponse',
      args: {
        username: nonNull('String'),
      },
      resolve(_root, args, { models }) {
        return forgotPassword(args.username).fork(
          (x) => x,
          (y) => y
        );
      },
    });
  },
});

export const ChangePasswordMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('changePassword', {
      type: 'FindUserResponse',
      args: {
        currentPassword: nonNull('String'),
        newPassword: nonNull('String'),
        id: nonNull('Int'),
      },
      resolve(_root, args, { models }) {
        return changePassword(args).fork(
          (x) => x,
          (y) => y
        );
      },
    });
  },
});
