import {
  mutationField,
  nonNull,
  floatArg,
  booleanArg,
  stringArg,
  intArg,
} from 'nexus';
import { requestLoan, addPayment } from '../../helpers/LoansAndPayment';

export const RequestLoan = mutationField('requestLoan', {
  type: 'LoanResponse',
  args: {
    amount: nonNull(floatArg()),
    payMentDate: nonNull(stringArg()),
    updatorId: nonNull(intArg()),
  },
  resolve(_root, args, {}) {
    return requestLoan(args);
  },
});

export const AddPayment = mutationField('addPayment', {
  type: 'PaymentResponse',
  args: {
    amount: nonNull(floatArg()),
    balance: nonNull(floatArg()),
    loanId: nonNull(intArg()),
    updatorId: nonNull(intArg()),
  },
  resolve(_root, args, {}) {
    return addPayment(args);
  },
});
