import { Roles } from '@prisma/client';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { db } from '../db';
import { PrismaClient, User } from '@prisma/client';
import { Task } from '../utils/monads';
import { validateLogin } from '../utils/validator';

const secret = process.env.TOKEN_SECRET;
export interface ILoginResponse {
  id: number;
  roleId: 'ADMIN' | 'USER';
}

export interface ILoginInput {
  password: string;
  email: string;
}

export interface UserMod extends User {
  roles: Roles;
}

const findUser =
  (db: PrismaClient) =>
  ({ email }: { email: string }) =>
    Task<UserMod>(
      async (rej, res) =>
        await db.user
          .findFirst({
            where: {
              email,
            },
            include: {
              roles: true,
            },
          })
          .then((user) =>
            user ? res(user) : rej({ message: 'User not found' })
          )
          .catch((err) => rej(err))
    );

const comparePassword =
  (inputPassword: string) =>
  ({ password, id, ...others }: UserMod) => {
    return Task<UserMod>((rej, res) =>
      bcrypt
        .compare(inputPassword, password)
        .then((isMatch) => {
          return isMatch
            ? res({ id, ...others })
            : rej({ message: 'Invalid Credentials' });
        })
        .catch((err) => rej({ message: 'Server error' }))
    );
  };

const signToken =
  (secret: string | undefined) =>
  ({ id, roles, fname }: UserMod) =>
    Task<{ token: string }>(async (rej, res) => {
      try {
        return secret
          ? res({
              token: jwt.sign({ id, roles, fname }, secret, {
                expiresIn: '20h',
              }),
            })
          : rej({ message: 'secret is required' });
      } catch (error) {
        return rej({ message: 'Token not signed' });
      }
    });

const handleLogin = ({ password, email }: ILoginInput) =>
  Task.of({ password, email })
    .chain(validateLogin)
    .chain(findUser(db))
    .chain(comparePassword(password))
    .chain(signToken(secret));
export { handleLogin };
