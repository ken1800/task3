import { makeSchema, fieldAuthorizePlugin } from 'nexus';
import { join } from 'path';
import * as types from './graphql';

export const schema = makeSchema({
  nonNullDefaults: {
    input: true,
    output: true,
  },
  types,
  plugins: [fieldAuthorizePlugin()],
  outputs: {
    typegen: join(__dirname, '..', 'nexus-typegen.ts'),
    schema: join(__dirname, '..', 'schema.graphql'),
  },
  contextType: {
    module: join(__dirname, './context.ts'),
    export: 'ContextModule',
  },
});
