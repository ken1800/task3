export {
  LoanResponse,
  LoanResult,
  PaymentResult,
  PaymentResponse,
} from './Types';

export { RequestLoan, AddPayment } from './Mutation';
export { getUserLoans, getUserPayment } from './Query';
