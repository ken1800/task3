/** @format */

import { PrismaClient, User, Prisma } from '@prisma/client';
import { Task } from '../../utils/monads';
import { sendEmail } from '../email.helper';
import { db } from '../../db';
import { decodeToken, IDelete, IBatchMany, signToken } from '../common.helper';
import bcrypt from 'bcrypt';
import { validateCreateUser } from '../../utils/validator';
import generatePassword from 'password-generator';

const CLIENT_URL = process.env.CLIENT_URL;
const TOKEN_SECRET = process.env.TOKEN_SECRET;

export interface IUpdateUser extends Prisma.UserUncheckedCreateInput {
  id: number;
}

export interface IUserFilters {
  roleId?: number;
  fname?: string;
  lname?: string;
  isActive?: boolean;
}

export interface IUpdateRole {
  id: number;
  roleId?: number | null | undefined;
  updatorId?: number;
}

export interface ISendVerificationEmail {
  email: string;
  id: number;
}

export interface IUpdatePassword {
  id: number;
  hashedPassword: string;
}

const hashPassword = (password: string) => {
  return Task<string>((rej, res) => {
    try {
      return res(bcrypt.hashSync(password, 10));
    } catch (error) {
      return rej({ message: 'Failed to hash password' });
    }
  });
};

export const findUserById = (id: number) => {
  return Task<User>(async (rej, res: (x: User) => User) => {
    try {
      const user = await db.user.findFirst({ where: { id } });
      return user ? res(user) : rej({ message: 'user not found' });
    } catch (err: any) {
      return rej({ message: err.message });
    }
  });
};

export const findUserByUserName = (db: PrismaClient) => (email: string) => {
  return Task<User>(async (rej, res: (x: User) => User) => {
    try {
      const user = await db.user.findFirst({ where: { email } });
      return user ? res(user) : rej({ message: 'user not found' });
    } catch (err: any) {
      return rej({ message: err.message });
    }
  });
};

export const findManyUsers = (db: PrismaClient) => (filters: IUserFilters) => {
  return Task<User[]>(async (rej, res: (x: User[]) => User[]) => {
    try {
      const users = await db.user.findMany({
        where: {
          ...filters,
        },
        include: {
          roles: true,
        },
      });
      return res(users);
    } catch (err: any) {
      return rej({ message: err.message });
    }
  });
};

export const CreateUser = (
  createUserInput: Prisma.UserUncheckedCreateInput
) => {
  return Task<User>(async (rej, res: (x: User) => User) => {
    try {
      const newUser = await db.user.create({
        data: { ...createUserInput, isActive: false },
      });

      return res(newUser);
    } catch (err: any) {
      return rej({ message: err.message });
    }
  });
};

export const UpdateUserRole = (updatedRoleInput: IUpdateRole) => {
  const { id, roleId } = updatedRoleInput;

  return db.user
    .update({
      where: { id },
      data: { roleId, isActive: true, updatedAt: new Date() },
      include: {
        roles: true,
      },
    })
    .then((result) => result);
};

export const UpdateDBPassword =
  (db: PrismaClient) => (updatePasswordInput: IUpdatePassword) => {
    const { id, hashedPassword } = updatePasswordInput;

    return Task<User>(async (rej, res: (x: User) => User) => {
      try {
        const updatedUser = await db.user.update({
          where: { id },
          data: { password: hashedPassword },
        });
        return res(updatedUser);
      } catch (err: any) {
        return rej({ message: err.message });
      }
    });
  };

export const toggleActivateUser =
  ({ activate }: { activate: boolean }) =>
  ({ ids }: { ids: number[] }) => {
    return Task<IBatchMany>((rej, res) => {
      try {
        return db.user.updateMany({
          where: {
            AND: [
              {
                id: {
                  in: ids,
                },
              },
            ],
          },
          data: {
            isActive: activate,
            roleId: !activate ? null : 0,
            updatedAt: new Date(),
          },
        });
      } catch (error: any) {
        return rej({ message: error.message });
      }
    });
  };

const createVerificationLink = (token: string) => {
  return `${CLIENT_URL}?token=${token}`;
};

const createVerificationToken = (id: number) => {
  return signToken({ payload: { id }, expiresIn: '1 days' }).map(
    createVerificationLink
  );
};

export const sendVerificationEmail = ({
  email,
  id,
}: ISendVerificationEmail) => {
  return createVerificationToken(id).chain((verificationLink) =>
    sendEmail({ text: verificationLink, to: email })
  );
};

export const createUser = (createUserInput: Prisma.UserUncheckedCreateInput) =>
  Task.of(createUserInput)
    .chain(CreateUser)
    .chain(({ email, id }) => sendVerificationEmail({ email, id }));

//updates user upon verifying via update link sent to their email
export const userVerification = (token: string) => {
  return decodeToken<IDelete>({ secret: TOKEN_SECRET, token }).chain(
    toggleActivateUser({ activate: true })
  );
};

export const createNewUser = (
  createUserInput: Prisma.UserUncheckedCreateInput
) => {
  return Task.of(createUserInput)
    .chain(validateCreateUser)
    .chain(() => hashPassword(createUserInput.password))
    .chain((hashedPassword) =>
      createUser({ ...createUserInput, password: hashedPassword })
    );
};

export const UpdateUserPassword = (updatePasswordInput: IUpdatePassword) => {
  return hashPassword(updatePasswordInput.hashedPassword).chain(
    (hashedPassword) =>
      UpdateDBPassword(db)({ ...updatePasswordInput, hashedPassword })
  );
};

const hashAndReturnUser = (password: string) => (user: User) => {
  return hashPassword(password).map((hash) => ({
    hashedPassword: hash,
    id: user.id,
  }));
};

//find user - gen password - sendEmail - hashPassword - update user password
export const forgotPassword = (email: string) => {
  const newPassword = generatePassword();
  return findUserByUserName(db)(email)
    .chain(hashAndReturnUser(newPassword))
    .chain(UpdateDBPassword(db))
    .chain((user) =>
      sendEmail({
        text: `new Password: ${newPassword}`,
        to: user.email,
        subject: 'Password Reset',
      })
    );
};

export interface IChangePassword {
  currentPassword: string;
  newPassword: string;
  id: number;
}

export const comparePassword = (user: User, currentPassword: string) => {
  return Task<User>((rej, res) => {
    return bcrypt
      .compare(currentPassword, user.password)
      .then((isMatch) => {
        return isMatch
          ? res(user)
          : rej({ message: 'Incorrect Current Password' });
      })
      .catch(() => {
        return rej({ message: 'Incorrect Current Password' });
      });
  });
};

export const changePassword = ({
  currentPassword,
  newPassword,
  id,
}: {
  currentPassword: string;
  newPassword: string;
  id: number;
}) => {
  return findUserById(id)
    .chain((user) => comparePassword(user, currentPassword))
    .chain(() => hashPassword(newPassword))
    .chain((hashedPassword) => UpdateDBPassword(db)({ hashedPassword, id }));
};
