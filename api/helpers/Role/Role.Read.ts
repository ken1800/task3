import { Rights } from '@prisma/client';
import { db } from '../../db';

export interface IRolesFilter {
  id?: number;
  name?: string;
  rights?: Rights;
  createdAt?: Date;
  updatedAt?: Date;
  isActive?: boolean;
}

export const findOneRole = async (id: number) => {
  return (
    (await db.roles.findFirst({ where: { id } })) || new Error('Role Not Found')
  );
};

export const getRoles = async ({ rights, ...others }: IRolesFilter) =>
  await db.roles.findMany({
    where: {
      ...others,
      isActive: true,
    },
  });
