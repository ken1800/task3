import { App as ApolloServer } from './server';

require('dotenv').config();

ApolloServer.listen({ port: process.env.PORT || 4001 }, (): void =>
  console.log(
    `\n🚀      GraphQL is now running on http://localhost:4000/graphql`
  )
);

export { ApolloServer };
