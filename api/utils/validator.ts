import Joi, { Schema } from 'joi';
import { ILoginInput } from '../helpers/Auth.helper';
// import { IUpdateRawMaterialInput } from '../helpers/RawMaterial';

import { Task } from './monads';
import { Prisma } from '@prisma/client';

export const validator =
  <T>(input: T) =>
  (schema: Schema) => {
    const { value, error } = schema.validate(input);
    return error ? Task.rejected({ message: error.message }) : Task.of(input);
  };

// AUTH

export const loginValidatorSchema = Joi.object({
  password: Joi.string().min(6).message('invalid password'),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ['com', 'net', 'ke'] },
    })
    .message('invalid username'),
});

export const createUserValidatorSchema = Joi.object({
  password: Joi.string()
    .min(6)
    .message('Password length Should be greater than 6'),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ['com', 'net', 'ke'] },
    })
    .message('invalid username'),
  updatorId: Joi.number(),
  lname: Joi.string().max(15),
  fname: Joi.string().max(15),
});

export const validateCreateUser = (
  userInput: Prisma.UserUncheckedCreateInput
) => validator<ILoginInput>(userInput)(createUserValidatorSchema);

export const validateLogin = (userInput: ILoginInput) =>
  validator<ILoginInput>(userInput)(loginValidatorSchema);
