/** @format */

import { AuthModel, AuthModelType } from './Auth';

export type Models = {
  Auth: AuthModelType;
};

export const models: Models = {
  Auth: AuthModel,
};
