import { extendType, nullable, intArg, nonNull } from 'nexus';
import { clean } from '../../helpers/common.helper';
import { findManyUsers, findUserById } from '../../helpers/User';

export const fetchUsers = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.list.field('users', {
      type: 'FindUserResponse',
      authorize: (root, args, { user }) => {
        return user?.roles?.rights?.some((right) => ['ALL'].includes(right));
      },
      args: {
        roleId: nullable(intArg()),
      },
      resolve: (_root, args, { models, user }) => {
        return findManyUsers(clean(args));
      },
    });
  },
});

export const fetchUserById = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('singleUser', {
      type: 'FindUserResponse',
      authorize: (root, args, { user }) => {
        return user?.roles?.rights?.some((right) => ['ALL'].includes(right));
      },
      args: {
        id: nonNull('Int'),
      },
      resolve: (_root, { id }, { models }) => {
        return findUserById(id).fork(
          (x) => x,
          (y) => y
        );
      },
    });
  },
});
