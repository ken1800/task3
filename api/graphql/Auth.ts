import {
  objectType,
  extendType,
  stringArg,
  nonNull,
  enumType,
  unionType,
} from 'nexus';
export const LoginResult = objectType({
  name: 'LoginResult',
  definition(t) {
    t.string('token');
  },
});

export const Error = objectType({
  name: 'Error',
  definition(t) {
    t.string('message');
  },
});

export const AuthResponse = unionType({
  name: 'AuthResponse',
  description: 'Returns token if login success else returns error',
  resolveType: (data) => {
    const __typename = 'token' in data ? 'LoginResult' : 'Error';
    return __typename;
  },
  definition(t) {
    t.members('LoginResult', 'Error');
  },
});

export const Role = enumType({
  name: 'Role',
  members: ['ADMIN', 'USER'],
  description: 'List of available Roles in the system',
});

export const LoginMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('login', {
      type: 'AuthResponse',

      args: {
        email: nonNull(stringArg()),
        password: nonNull(stringArg()),
      },
      resolve(_root, args, { models, user }) {
        return models.Auth.login(args);
      },
    });
  },
});
