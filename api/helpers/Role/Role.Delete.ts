import { db } from '../../db';

export const deleteRole = (ids: number[]) =>
  db.roles
    .updateMany({
      where: {
        id: {
          in: ids,
        },
      },

      data: {
        isActive: false,
      },
    })
    .then((result) => result)
    .catch((error) => new Error(error));
