import { db } from '../../db';
export interface IUserFilters {
  roleId?: number;
  fname?: string;
  lname?: string;
  isActive?: boolean;
}
export const findManyUsers = (filters: IUserFilters) =>
  db.user.findMany({
    where: {
      ...filters,
    },
    include: {
      roles: true,
    },
  });
