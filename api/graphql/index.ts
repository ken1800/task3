/** @format */

import * as Auth from './Auth';
import * as User from './User';
import * as Roles from './Role';
import * as Custom from './Custom';
import * as LoansPayment from './LoansPayment';

export { Auth, User, Roles, Custom, LoansPayment };
