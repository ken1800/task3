import { handleLogin, ILoginInput } from '../helpers/Auth.helper';

export type AuthModelType = {
  login: (input: ILoginInput) => { token: string };
};

export const AuthModel = {
  login: ({ password, email }: ILoginInput) => {
    return handleLogin({ password, email }).fork(
      (x) => x,
      (x) => x
    );
  },
};
