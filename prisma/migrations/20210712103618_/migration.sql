/*
  Warnings:

  - Added the required column `updatorId` to the `Payment` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Payment" ADD COLUMN     "updatorId" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "Payment" ADD FOREIGN KEY ("updatorId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
