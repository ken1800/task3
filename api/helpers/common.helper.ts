/** @format */

import { Task } from '../utils/monads';
import jwt from 'jsonwebtoken';
import { ProductStatus } from '@prisma/client';
require('dotenv').config();

const TOKEN_SECRET = process.env.TOKEN_SECRET!;

interface ISignToken {
  payload: {};
  expiresIn: string;
}

export const signToken = ({ payload, expiresIn }: ISignToken) => {
  return Task<string>((rej, res) => {
    try {
      const token = jwt.sign({ ...payload }, TOKEN_SECRET, {
        expiresIn,
      });
      return res(token);
    } catch (error) {
      return rej({ message: 'Failed to sign token' });
    }
  });
};

export const decodeToken = <T>({
  token,
  secret,
}: {
  token: string;
  secret: string | undefined;
}) =>
  Task<T>((rej, res) => {
    try {
      if (secret) {
        const decoded = jwt.verify(token, secret);
        return decoded ? res(decoded) : rej({ message: 'Invalid token' });
      }
    } catch (error) {
      return rej({ message: error.message });
    }
  });

export function clean(obj: any): {} {
  for (var propName in obj) {
    if (obj[propName] === null || obj[propName] === undefined) {
      delete obj[propName];
    }
  }
  return obj;
}

export interface IDelete {
  ids: number[];
  updatorId?: number;
  approverName?: string;
}

export interface IQuotationDelete {
  ids: number[];
  updatorId?: any;
  approverName?: string;
  status: ProductStatus;
}
export interface IBatchMany {
  count: number;
}
