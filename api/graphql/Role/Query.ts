import { Rights } from './Type';
import { extendType, nullable, list, stringArg, nonNull, intArg } from 'nexus';
import { clean } from '../../helpers/common.helper';
import { getRoles, findOneRole } from '../../helpers/Role';

export const RoleesQuery = extendType({
  type: 'Query',
  definition(t) {
    t.list.nullable.field('roles', {
      type: 'RoleResponse',

      args: {
        rights: nullable(list(Rights)),
        name: nullable(stringArg()),
      },
      resolve(_root, args, { models }) {
        return getRoles(clean(args));
      },
    });
  },
});

export const FindOneRoleQuery = extendType({
  type: 'Query',
  definition(t) {
    t.nonNull.field('findOneRole', {
      type: 'RoleResponse',

      args: {
        id: nonNull(intArg()),
      },
      resolve(_root, { id }, { models }) {
        return findOneRole(id);
      },
    });
  },
});
