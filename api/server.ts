import { ApolloServer } from 'apollo-server';
import { schema } from './schema';
import { models } from './models';
import { decodeToken } from './helpers/common.helper';

const TOKEN_SECRET = process.env.TOKEN_SECRET;

export const App = new ApolloServer({
  schema,
  context: async ({ req }) => {
    const token = req.headers.authorization || '';
    const user = decodeToken({ token, secret: TOKEN_SECRET }).fork(
      (x) => x,
      (x) => x
    );

    return { models, user };
  },
});
