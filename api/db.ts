import { PrismaClient } from '@prisma/client';
export const db = new PrismaClient({
  log: [
    {
      level: 'query',
      emit: 'event',
    },
  ],
});

// db.$on('query', (e) => {
//   console.log('Query: ' + e.query);
//   console.log('Duration: ' + e.duration + 'ms');
// });
