/** @format */

import { Prisma } from "@prisma/client";
import { db } from "../../db";

export const createRole = (rolesInput: Prisma.RolesCreateInput) =>
  db.roles
    .create({
      data: {
        ...rolesInput,
      },
    })
    .then((res) => res)
    .catch((err) => ({ message: "error adding role" }));
