Stack Used: Nodejs , GraphQL, Apollo-Server, Nexus, Prisma, Postgres,Typescript - This is a graphql server endpoint

SETUP:

 - Create a `.env` on you root folder.

 -  Create a postgres database and add the connection string in the `.env` with this format `DATABASE_URL=postgres://username:password@localhost:5432/eclectics`.

 - on the root-folder, run `npx-prisma-migrate`.This will create your database tables.

 - run `npm run dev` this will start the server at port : `localhost:4001`




