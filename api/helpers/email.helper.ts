import { Task } from '../utils/monads';

const sgMail = require('@sendgrid/mail');

interface IEmailMessage {
  to: string;
  text: string;
  subject?: string;
  html?: string;
}

export const sendEmail = ({ text, to, subject, html }: IEmailMessage) => {
  const messageVars = {
    text,
    to,
    subject: subject || 'verify',
    from: 'developers@keitt.co.ke',
    html,
  };
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  return Task<{ EmailSuccess: boolean }>((rej, res) =>
    sgMail
      .send(messageVars)
      .then(() => res({ EmailSuccess: true }))
      .catch((error: { response: { body: string } }) => {
        return rej({ error, message: 'Failed to send email' });
      })
  );
};
