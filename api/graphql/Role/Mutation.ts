import { Rights } from './Type';
import {
  extendType,
  list,
  nonNull,
  stringArg,
  nullable,
  intArg,
  booleanArg,
} from 'nexus';
import { clean } from '../../helpers/common.helper';
import {
  createRole,
  deleteRole,
  updateRole,
  updateUsersRole,
} from '../../helpers/Role';

export const AddRoleMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('addRole', {
      type: 'RoleResponse',

      args: {
        rights: list(nonNull(Rights)),
        name: nonNull(stringArg()),
      },
      resolve(_root, args, { models }) {
        return createRole(args);
      },
    });
  },
});

export const UpdateRoleMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateRole', {
      type: 'RoleResponse',

      args: {
        rights: nullable(list(Rights)),
        name: nullable(stringArg()),
        id: nonNull(intArg()),
        isActive: nullable(booleanArg()),
      },
      resolve(_root, args, { models }) {
        return updateRole({ ...clean(args), id: args.id });
      },
    });
  },
});

export const DeleteRoleMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('deleteRole', {
      type: 'BatchPayloadResponse',

      args: {
        ids: list(nonNull(intArg())),
      },

      resolve(_root, { ids }, { models }) {
        return deleteRole(ids);
      },
    });
  },
});

export const UpdateUsersRightsMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.nonNull.field('updateUserRights', {
      type: 'User',

      args: {
        otherRights: list(Rights),
        id: nonNull(intArg()),
      },
      resolve(_root, { id, otherRights }, { models }) {
        return updateUsersRole(id, otherRights);
      },
    });
  },
});
