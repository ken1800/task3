import { objectType, enumType, unionType } from 'nexus';

export const RolesResult = objectType({
  name: 'RolesResult',
  definition(t) {
    t.int('id');
    t.list.field('rights', {
      type: 'Rights',
    });
    t.nullable.field('User', {
      type: 'User',
    });
    t.string('name');
    t.date('createdAt');
    t.date('updatedAt');
    t.boolean('isActive');
  },
});

export const Rights = enumType({
  name: 'Rights',
  members: [
    'APPROVE_LOAN',
    'REJECT_LOAN',
    'DISBURSE_LOAN',
    'VIEW_USERS',
    'ALL',
    'NONE',
  ],
  description: 'Rights available type',
});

export const RoleResponse = unionType({
  name: 'RoleResponse',
  description: 'Role response model',
  resolveType: (data) => {
    const __typename = 'id' in data ? 'RolesResult' : 'Error';

    return __typename;
  },
  definition(t) {
    t.members('RolesResult', 'Error');
  },
});
