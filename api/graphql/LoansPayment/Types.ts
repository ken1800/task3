import { objectType, unionType } from 'nexus';

export const LoanResult = objectType({
  name: 'LoanResult',
  definition(t) {
    t.int('id');
    t.int('updatorId');
    t.float('amount');
    t.boolean('isApproved');
    t.date('payMentDate');
    t.date('createdAt');
    t.date('updatedAt');
    t.boolean('isActive');
  },
});

export const LoanResponse = unionType({
  name: 'LoanResponse',
  description: 'Loan Response',
  resolveType: (data) => {
    const __typename = 'id' in data ? 'LoanResult' : 'Error';
    return __typename;
  },
  definition(t) {
    t.members('LoanResult', 'Error');
  },
});

export const PaymentResult = objectType({
  name: 'PaymentResult',
  definition(t) {
    t.int('id');
    t.float('amount');
    t.float('balance');
    t.int('loanId');
    t.int('updatorId');
    t.date('createdAt');
    t.date('updatedAt');
    t.boolean('isActive');
  },
});

export const PaymentResponse = unionType({
  name: 'PaymentResponse',
  description: 'Payment Response',
  resolveType: (data) => {
    const __typename = 'id' in data ? 'PaymentResult' : 'Error';
    return __typename;
  },
  definition(t) {
    t.members('PaymentResult', 'Error');
  },
});
