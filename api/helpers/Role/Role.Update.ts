import { Prisma, Rights } from '@prisma/client';
import { db } from '../../db';

export interface IUpdateRolesInput extends Prisma.RolesUpdateInput {
  id: number;
  isActive?: boolean;
}

export const updateRole = ({ id, ...other }: IUpdateRolesInput) =>
  db.roles.update({
    where: {
      id,
    },
    data: {
      ...other,
    },
  });

export const updateUsersRole = (id: number, otherRights: Rights[]) =>
  db.user.update({
    where: {
      id,
    },
    data: {
      otherRights: otherRights,
    },
    include: {
      roles: true,
    },
  });
