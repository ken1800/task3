export interface TaskT<A> {
  map: <B>(f: (a: A) => B) => TaskT<B>;
  chain: <B>(f: (a: A) => TaskT<B>) => TaskT<B>;
  fork: (rej: Rej, res: (x: A) => A) => A;
  ap: (x: TaskT<any>) => TaskT<any>;
}

type Rej = (x: ErrorMessage) => ErrorMessage;

interface ErrorMessage {
  message: string;
  error?: any;
}

export const Task = <A>(
  fork: (rej: Rej, res: (x: any) => any) => any
): TaskT<A> => ({
  fork,
  ap: (other) =>
    Task((rej, res) => fork(rej, (f) => other.fork(rej, (x) => res(f(x))))),
  map: (f) =>
    Task((rej: Rej, res: (x: any) => any) => fork(rej, (x) => res(f(x)))),
  chain: (f) =>
    Task((rej: Rej, res: (x: any) => any) =>
      fork(rej, (x: A) => f(x).fork(rej, res))
    ),
  // concat: (other) =>
  //   Task((rej, res) =>
  //     fork(rej, (x) =>
  //       other.fork(rej, (y) => {
  //         // console.log("X", x, "Y", y);
  //         return res(x.concat(y));
  //       })
  //     )
  //   ),
  // fold: (f, g) =>
  //   Task((rej, res) =>
  //     fork(
  //       (x) => f(x).fork(rej, res),
  //       (x) => g(x).fork(rej, res)
  //     )
  //   ),
});

Task.of = <A>(x: A): TaskT<A> =>
  Task((rej: Rej, res: (x: A) => TaskT<A>) => res(x));
Task.rejected = (x: ErrorMessage): TaskT<any> =>
  Task((rej: Rej, res: any) => rej(x));
